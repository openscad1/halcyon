//cube([10, 10, 10], center = true);

include <cutgears/gears.scad>;
help_gears();

cycloid = false;

teeth = 18; 
axle_diameter = 6.1;
gear_module = 2;
gear_height = 6;
bevel_depth = 1.0;

hub_length = 17;
axle_housing_diameter = 8;


label1 = str("b",bevel_depth, "/", "a",axle_housing_diameter);
label2 = ":D";


// true
// false
crop_upper = false;
crop_lower = false;


do_label = true;

module foo() {
    difference() {
        union() {
            difference() {
                rotate([0, 0, (360 / teeth) / 2]) {
                    gear(m = gear_module, z = teeth, x = -0, h = gear_height);
                }
                translate([15 - 1.2, 0, 0]) {
                        cube([30, 60, 30], center = true);
                    }
            }

            cylinder(d = axle_housing_diameter, h = hub_length, center = true);
        }


        // axle
        difference() {
            cylinder(h = hub_length + 2, d = axle_diameter, center = true);
            translate([15 + 3, 0, 0]) {
                translate([-bevel_depth, 0, 0]) {
                        cube([30, 30, hub_length + 1], center = true);
                    }
            }
        }
    }
}


difference() {
    foo();
    if(crop_lower) {
        translate([0, 0, -50]) {
            cube([100, 100, 100], center = true);
        }
    }
    if(crop_upper) {
        translate([0, 0, 50]) {
            cube([100, 100, 100], center = true);
        }
    }

    if(do_label) {

        color("red") {
            rotate([0, 0, 90])
            translate([0, 0, (gear_height / 2) - 1]) 
            linear_extrude(2) {
                fontsize = 4;
                translate([0, 9, 0]) {
                    text(str(label1, " ", label2), valign = "center", halign = "center", size = fontsize);
                }  
            }
        }

    }
}

